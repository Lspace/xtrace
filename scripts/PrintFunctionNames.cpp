//===- PrintFunctionNames.cpp ---------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include "clang/Frontend/FrontendPluginRegistry.h"
#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Sema/Sema.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "llvm/Support/raw_ostream.h"
#include <map>

using namespace clang;

namespace {

class ReturnCollector : public clang::RecursiveASTVisitor<ReturnCollector> {
public:
  using Returns = std::vector<clang::ReturnStmt *>;

  static Returns collect(clang::CXXMethodDecl *MD) {
    ReturnCollector ActualCollector;
    ActualCollector.TraverseDecl(MD);
    return ActualCollector.Visited;
  }

  bool VisitReturnStmt(clang::ReturnStmt *RS) {
    Visited.push_back(RS);
    return true;
  }

private:
  ReturnCollector() = default;
  Returns Visited;
};

class AstVisitor : public RecursiveASTVisitor<AstVisitor> {
    public: 
        AstVisitor(Rewriter *R, SourceManager *mgr) : rewriter_(R), smgr_(mgr) {} 

        void handleReturnStmt(Stmt *stmt)
        {
            ReturnStmt *rs = cast<ReturnStmt>(stmt);
            std::string loc = stmt->getBeginLoc().printToString(rewriter_->getSourceMgr());
            std::string funcName = funcList.back();
            std::string ret = "\nprintf(\"[return] " + loc + " " + funcName + "()\\n\");\n";
            rewriter_->InsertText(rs->getBeginLoc(), ret, true, true);
        }


        bool VisitStmt(Stmt *s)
        {
            if (!smgr_->isInMainFile(s->getBeginLoc())) return true;
            if (isa<ReturnStmt>(s)) handleReturnStmt(s);
            return true;
        }


        bool TraverseCXXMethodDecl(CXXMethodDecl *D) {
            if (!smgr_->isInMainFile(D->getBeginLoc())) return true;
            if (D->getBody() == nullptr) return true; 
            if (!funcList.empty()) funcList.pop_back();
    
            std::string loc = D->getBeginLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
            std::string funcName = D->getNameInfo().getName().getAsString();
            funcList.push_back(funcName);
            std::string enter = "\nprintf(\"[enter] " + loc + " " + funcName + "()\\n\");\n";
            rewriter_->InsertText(D->getBody()->getBeginLoc().getLocWithOffset(1), enter, true, true);
            QualType QT = D->getReturnType();
            auto ReturnStmts = ReturnCollector::collect(D);
            for (auto *Return : ReturnStmts) {
              handleReturnStmt(Return);
            }
            if (QT.getAsString() == "void") {
              loc = D->getBody()->getEndLoc().printToString(rewriter_->getSourceMgr());
              std::string ret = "\nprintf(\"[return] " + loc + " " +  funcName + "()\\n\");\n";
              rewriter_->InsertText(D->getBody()->getEndLoc(), ret, false, true);
            }
            return true;
        }

        bool VisitFunctionDecl(FunctionDecl *f)
        {
            if (!smgr_->isInMainFile(f->getBeginLoc())) return true;
            if (f->getBody() == nullptr) return true;

            if (!funcList.empty()) funcList.pop_back();
            std::string funcName = f->getNameInfo().getName().getAsString(); 
            funcList.push_back(funcName); 
            std::string loc = f->getBeginLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
            
            std::string enter = "\nprintf(\"[enter] " + loc + " " + funcName + "()\\n\");\n";
            rewriter_->InsertText(f->getBody()->getBeginLoc().getLocWithOffset(1), enter, true, true);
            QualType QT = f->getReturnType();
            if (QT.getAsString() == "void") {
              loc = f->getBody()->getEndLoc().printToString(rewriter_->getSourceMgr());
              std::string ret = "\nprintf(\"[return] " + loc + " " + funcName + "()\\n\");\n";
              rewriter_->InsertText(f->getBody()->getEndLoc(), ret, false, true);
            }
            return true;
        }

    private:
        Rewriter *rewriter_;
        SourceManager *smgr_;
        std::vector<std::string> funcList;
};

class PrintFunctionsConsumer : public ASTConsumer {
  CompilerInstance &Instance;
  Rewriter &rewriter;

public:
  PrintFunctionsConsumer(CompilerInstance &Instance, Rewriter &writer)
      : Instance(Instance), rewriter(writer) {}

  bool HandleTopLevelDecl(DeclGroupRef DG) override {
    SourceManager &sm = Instance.getSourceManager();
    AstVisitor v(&rewriter, &sm); 
    for (DeclGroupRef::iterator i = DG.begin(), e = DG.end(); i != e; ++i) { 
      v.TraverseDecl(*i);
    }
    return true;
  }

  void HandleTranslationUnit(ASTContext& context) override { 
  }
};

class PrintFunctionNamesAction : public PluginASTAction { 
public:
    void EndSourceFileAction() override {
      SourceManager &sm = rewriter.getSourceMgr();
      llvm::StringRef filename = sm.getFileEntryForID(sm.getMainFileID())->getName(); 
      llvm::outs() << "########## processing file:" << filename << "\n";
      // rewriter.getEditBuffer(sm.getMainFileID()).write(llvm::outs());
      std::error_code ec;
      llvm::raw_fd_ostream os(filename, ec, llvm::sys::fs::OF_None);
      rewriter.getEditBuffer(sm.getMainFileID()).write(os);
    }

protected:
  Rewriter rewriter;

  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 llvm::StringRef) override {
    
    SourceManager &sm = CI.getSourceManager();
    rewriter.setSourceMgr(sm, CI.getLangOpts());
    return std::make_unique<PrintFunctionsConsumer>(CI, rewriter);
  }

  bool ParseArgs(const CompilerInstance &CI,
                 const std::vector<std::string> &args) override {

    return true;
  }

};

}

static FrontendPluginRegistry::Add<PrintFunctionNamesAction>
X("print-fns", "print function names");
