#!/bin/bash

srcPath=$1

# strace -e trace=write -s1000 -fp 18386 2>&1 | grep -o '".\+[^"]"'
files=`find $srcPath -name '*.cpp'`
for file in $files; do
    echo $file
    ./ctracer -f $file -l printf -i includes
done
