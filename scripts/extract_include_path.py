import sys,json

fname = sys.argv[1]
fobj = open(fname, 'r')
jdata = json.load(fobj)
cdict = {}

for entry in jdata:
    cmd = entry['command']
    buf = cmd.split(' ')
    for b in buf:
        if b.startswith('-I'):
            if 'test' in b: continue
            header = b.split('-I')[1]
            cdict[header] = 1
fobj.close()

headerbase = 'headerSearchOptions.AddPath("%s", clang::frontend::Angled, false, false);'
for header in cdict.keys():
    print(header)
