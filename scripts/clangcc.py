
import json, sys, os

fobj = open(sys.argv[1])
jobj = json.load(fobj)

lib = '/home/osboxes/pkgs/llvm-build/lib/PrintFunctionNames.so'
cmdbase0 = '/usr/bin/clang++ -fsyntax-only -Xclang -load -Xclang %s -Xclang -plugin -Xclang print-fns -Xclang -plugin-arg-print-fns ' % lib
cmdbase1 = '/usr/bin/clang -fsyntax-only -Xclang -load -Xclang %s -Xclang -plugin -Xclang print-fns -Xclang -plugin-arg-print-fns ' % lib

cnt = 0
for itr in jobj:
    cmd = itr['command']
    os.chdir(itr['directory'])
    cmd = cmd.replace('/usr/bin/clang++ ', cmdbase0)
    cmd = cmd.replace('/usr/bin/clang ', cmdbase1)
    print(cmd)
    os.system(cmd)
    cnt += 1


