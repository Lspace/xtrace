#include <cstdio>
#include <memory>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <string>

#include "clang/AST/ASTConsumer.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"
#include "llvm/Support/Host.h"
#include "llvm/Support/raw_ostream.h"
#include "clang/Tooling/Tooling.h"
#include <fmt/core.h>
#include <CLI/CLI.hpp>


using namespace clang;

class AstVisitor : public RecursiveASTVisitor<AstVisitor> {
    public:
        static std::map<std::string, bool> targetStmt;
        static std::map<std::string, std::string> settings;

        AstVisitor(Rewriter *R, SourceManager *mgr) : rewriter_(R), smgr_(mgr) {}

        bool VisitStmt(Stmt *s)
        {
            if (!smgr_->isInMainFile(s->getBeginLoc())) return true;

            if (isa<ReturnStmt>(s)) handleReturnStmt(s);
            else if (isa<IfStmt>(s)) handleIfStmt(s);
            else if (isa<ForStmt>(s)) handleForStmt(s);
            else if (isa<WhileStmt>(s)) handleWhileStmt(s);

            return true;
        }

        void handleWhileStmt(Stmt *stmt)
        {
            WhileStmt *ws = cast<WhileStmt>(stmt);
            injectStmt(ws->getBody());
        }

        void handleReturnStmt(Stmt *stmt)
        {
            llvm::outs() << stmt->getStmtClassName() << "\n";
            ReturnStmt *rs = cast<ReturnStmt>(stmt);
            std::string loc = stmt->getBeginLoc().printToString(rewriter_->getSourceMgr());
            std::string funcName = funcList.back()->getNameInfo().getName().getAsString();
            std::string log = fmt::format("{}(\"{} [xreturn] {}\\n\");\n", settings["logger"], loc, funcName);
            llvm::outs() << log << "\n";
            rewriter_->InsertText(stmt->getBeginLoc(), log, true, true);
        }

        void handleForStmt(Stmt *stmt)
        {
            ForStmt *s = cast<ForStmt>(stmt);
            injectStmt(s->getBody());
        }

        void injectStmt(Stmt *stmt)
        {
            if (stmt && strcmp(stmt->getStmtClassName(), "CompoundStmt") == 0) {
                rewriter_->InsertText(stmt->getBeginLoc().getLocWithOffset(1), genCompundStmtLog(stmt), true, true);
            }
        }

        std::string genCompundStmtLog(Stmt *stmt)
        {
            std::string loc = stmt->getBeginLoc().printToString(rewriter_->getSourceMgr());
            return fmt::format("{}(\"{}\\n\");\n", settings["logger"], loc);
        }

        void handleIfStmt(Stmt *stmt)
        {
            IfStmt *s = cast<IfStmt>(stmt);
            injectStmt(s->getThen());
            injectStmt(s->getElse());
        }

        bool TraverseCXXMethodDecl(CXXMethodDecl *D) {
            if (AstVisitor::settings["verbose"] == "true") {
                llvm::outs() << "cxxmethod:" << D->getNameInfo().getName().getAsString() << "\n";
            }
            if (D->getBody() == nullptr) return true;
            std::string loc = D->getBeginLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
            std::string funcName = D->getNameInfo().getName().getAsString();
            std::string log = fmt::format("\n{}(\"{} [enter] {}\\n\");\n", settings["logger"], loc, funcName);
            rewriter_->InsertText(D->getBody()->getBeginLoc().getLocWithOffset(1), log, true, true);
            QualType QT = D->getReturnType();
            std::string TypeStr = QT.getAsString();
            if (TypeStr == "void") {
                std::string loc = D->getEndLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
                std::string log = fmt::format("{}(\"{} [return] {}\\n\");\n", settings["logger"], loc, funcName);
                rewriter_->InsertText(D->getBody()->getEndLoc(), log, false, true);
            }
            return true;
        }

        bool VisitFunctionDecl(FunctionDecl *f)
        {
            if (!smgr_->isInMainFile(f->getBeginLoc())) return true;

            if (!funcList.empty()) funcList.pop_back();
            funcList.push_back(f);
            std::string funcName = f->getNameInfo().getName().getAsString();
            if (AstVisitor::settings["verbose"] == "true") {
                llvm::outs() << "function:" << funcName << "\n";
            }
            std::string loc = f->getBeginLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
            std::string log = fmt::format("\n{}(\"{} [enter] {}\\n\");\n", settings["logger"], loc, funcName);
            if (f->getBody() == nullptr) return true;
            rewriter_->InsertText(f->getBody()->getBeginLoc().getLocWithOffset(1), log, true, true);
            QualType QT = f->getReturnType();
            std::string TypeStr = QT.getAsString();
            if (TypeStr == "void") {
                std::string loc = f->getEndLoc().getLocWithOffset(1).printToString(rewriter_->getSourceMgr());
                std::string log = fmt::format("{}(\"{} [return] {}\\n\");\n", settings["logger"], loc, funcName);
                rewriter_->InsertText(f->getBody()->getEndLoc(), log, false, true);
            }
            return true;
        }

    private:
        Rewriter *rewriter_;
        SourceManager *smgr_;
        std::vector<FunctionDecl*> funcList;
};

class Consumer : public ASTConsumer
{
    public:
        Consumer(Rewriter *R, SourceManager *smgr) : Visitor(R, smgr) {}

        virtual bool HandleTopLevelDecl(DeclGroupRef DR) {
            for (DeclGroupRef::iterator b = DR.begin(), e = DR.end(); b != e; ++b) {
                Visitor.TraverseDecl(*b);
            }
            return true;
        }
    private:
        AstVisitor Visitor;
};


std::map<std::string, bool> AstVisitor::targetStmt = {
    {"ForStmt", true},
    {"WhileStmt", true},
    {"CallExpr", true},
    {"ReturnStmt", true},
    {"IfStmt", true}
};

std::map<std::string, std::string> AstVisitor::settings = {
    {"logger", "printf"},
    {"verbose", "false"},
};


int main(int argc, char *argv[]) {
    CLI::App app{"fx compiler"};
    std::string fname, logger, verbose, includeFile;
    app.add_option("-f,--file", fname, "source file");
    app.add_option("-l,--logger", logger, "logger function");
    app.add_option("-v,--verbose", verbose, "verbose: true/false");
    app.add_option("-i,--include", includeFile, "include: includes");
    CLI11_PARSE(app, argc, argv);

    if (logger != "") AstVisitor::settings["logger"] = logger;
    if (verbose != "") AstVisitor::settings["verbose"] = verbose;

    CompilerInstance TheCompInst;
    TheCompInst.createDiagnostics();

    LangOptions &lo = TheCompInst.getLangOpts();
    lo.GNUMode = 1;
    lo.CPlusPlus11 = 1;
    lo.Bool = 1;

    auto TO = std::make_shared<TargetOptions>();
    TO->Triple = llvm::sys::getDefaultTargetTriple();
    TargetInfo *TI =
        TargetInfo::CreateTargetInfo(TheCompInst.getDiagnostics(), TO);
    TheCompInst.setTarget(TI);

    TheCompInst.createFileManager();
    FileManager &FileMgr = TheCompInst.getFileManager();
    TheCompInst.createSourceManager(FileMgr);
    SourceManager &SourceMgr = TheCompInst.getSourceManager();

    auto headerSearchOptions = TheCompInst.getHeaderSearchOptsPtr();
    std::ifstream includes(includeFile);
    std::string includeLine;
    while (std::getline(includes, includeLine)) {
        headerSearchOptions->AddPath(includeLine, clang::frontend::Angled, false, false);
    }

    TheCompInst.createPreprocessor(TU_Module);
    TheCompInst.createASTContext();

    Rewriter rewriter_;
    rewriter_.setSourceMgr(SourceMgr, TheCompInst.getLangOpts());

    const FileEntry *FileIn = FileMgr.getFile(fname).get();
    SourceMgr.setMainFileID(
            SourceMgr.createFileID(FileIn, SourceLocation(), SrcMgr::C_User));
    TheCompInst.getDiagnosticClient().BeginSourceFile(
            TheCompInst.getLangOpts(), &TheCompInst.getPreprocessor());

    Consumer TheConsumer(&rewriter_, &SourceMgr);

    ParseAST(TheCompInst.getPreprocessor(), &TheConsumer,
            TheCompInst.getASTContext());

    const RewriteBuffer *RewriteBuf =
        rewriter_.getRewriteBufferFor(SourceMgr.getMainFileID());

    std::string content = std::string(RewriteBuf->begin(), RewriteBuf->end());
    // rewriter_.getEditBuffer(SourceMgr.getMainFileID()).write(llvm::outs());
    FileIn->closeFile();
    // llvm::outs() << content << "\n";
    std::ofstream outFile(fname, std::ios::trunc);
    outFile << content;
    outFile.close();

    return 0;
}
